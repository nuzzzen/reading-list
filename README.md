# My personal tech reading list

![GitLab last commit](https://img.shields.io/gitlab/last-commit/nuzzzen/reading-list?gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge)

# DevOps - Culture/Organization
- **[The Phoenix Project](https://www.goodreads.com/book/show/17255186-the-phoenix-project)** (*Kim/Spafford/Behr*, 2013)
    > Allegorical novel that uses fiction to teach us about the advantages of modern IT culture (agile, CI/CD, etc.).

- **[The Unicorn Project](https://www.goodreads.com/book/show/44333183-the-unicorn-project)** (*Kim*, 2019)
    > Sequel to Phoenix Project, from the developers' perspective. 

- **[Accelerate](https://www.goodreads.com/book/show/35747076-accelerate)** (*Forsgren/Kim/Humble*, 2018)
    > Using a scientific approach to measure and improve performance in DevOps organizations.

- **[Team Topologies](https://www.goodreads.com/book/show/44135420-team-topologies)** (*Skelton/Pais*, 2019)
    > Book about team organization in the context of larger DevOps/Agile organizations.



# SRE/DevOps - Technological​​​​​​​

- **[Google's SRE Book](https://sre.google/sre-book/table-of-contents/)** (*Google Inc.*, 2016) 
    > Google was one of the first companies to define what business-IT alignment meant in practice, and went on to inform the concept of DevOps for a wider IT community. This book has been written by a broad cross-section of the very people who made that transition a reality.

- **[Site Reliability Engineering]()** (*edited by Beyer et al.*, 2016)
    > Google sharing their Site Reliability practises was one of the catalysts for the evolution of DevOps/SRE/etc. related work in recent years, and this book is a massive tome of knowledge that touches all points of interest, from monitoring to incident management.

- **[Enterprise Roadmap to SRE](https://sre.google/resources/practices-and-processes/enterprise-roadmap-to-sre)** (*Brookbank, McGhee*, 2022)
    > If you're a product owner or have a stake in reliable services and need to know more about SRE adoption, this report will methodically guide you through the process.

- **[Site Reliability Workbook](https://www.goodreads.com/book/show/39687146-the-site-reliability-workbook)** (*edited by Beyer et al.*, 2018)
    > The practical companion to the SRE book.

- **[Building Secure and Reliable Systems](https://www.goodreads.com/book/show/52362720-building-secure-and-reliable-systems)** (*Adkins et al*., 2020)
    > The third Google/O'Reilly book, oriented towards a more Security Engineer mindset, but still in the same style as the two previous ones.

- **[Seeking SRE](https://www.goodreads.com/book/show/41737449-seeking-sre)** (*edited by Blank-Edelman*, 2018)
    > Another collection of essays/articles, from a more varied base of writers than just googlers. 

- **[Kubernetes Up & Running](https://www.goodreads.com/book/show/26759355-kubernetes)** (*Hightower*, 2016), **[Terraform Up & Running](https://www.goodreads.com/book/show/42589303-terraform)** (*Brikman*, 2019) and others

- **[Ansible for DevOps](https://www.goodreads.com/book/show/27111284-ansible-for-devops)** (*Geerling*, 2015)
    > This and all of *Jeff Geerling's* work is generally a solid recommendation for learning Ansible

- **[Kubernetes Patterns](https://k8spatterns.com/)** (*B. Ibryam, R. Huß, 2023*)
    > This book provides common reusable patterns and principles for designing and implementing cloud native applications on Kubernetes.
    Each pattern includes a description of the problem and a Kubernetes-specific solution 

# Hybrid

- **[The DevOps Handbook](https://www.goodreads.com/book/show/26083308-the-devops-handbook)** (*Kim/Humble/Debois/Willis*, 2016)
    >the practical side to the Phoenix Project, how to implement DevOps Culture

- **[Continuous Delivery](https://www.goodreads.com/book/show/8686650-continuous-delivery)** (*Humble/Farley*, 2010)
    > The book that started it all. More of a reference book than something you read cover to cover, but an essential in any library.

- **[Time Management for System Administrators](https://www.goodreads.com/book/show/376236.Time_Management_for_System_Administrators)** (*Limoncelli*, 2005)
    > Time Management for System Administrators understands that an Sys Admin often has competing goals: the concurrent responsibilities of working on large projects and taking care of a user's needs. That's why it focuses on strategies that help you work through daily tasks, yet still allow you to handle critical situations that inevitably arise.

# Programming - Technological​​​​​​​

- **[Learning Python](https://www.goodreads.com/book/show/80435.Learning_Python)** (*Lutz, Ascher*, 2013)
    > Get a comprehensive, in-depth introduction to the core Python language with this hands-on book.

- **[Grokking Algorithms](https://www.goodreads.com/book/show/22847284-grokking-algorithms-an-illustrated-guide-for-programmers-and-other-curio)** (_Aditya Y. Bhargava_, 2024)
    > The algorithms you'll use most often as a programmer have already been discovered, tested, and proven. This book will prepare you for those pesky algorithms questions in every programming job interview and help you apply them in your day-to-day work.

- **[Learning Helm](https://www.oreilly.com/library/view/learning-helm/9781492083641/)** (*M. Butcher, M. Farina, J. Dolitsky, 2021*)
    > This practical guide shows you how to efficiently create, install, and manage the applications running inside your containers.



